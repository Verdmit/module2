﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            #region Блок объявления переменных
            int companiesNumber, tax, companyRevenue, age;

            string UserInput, first, second;

            double UserOutput3;

            // string OutputString;
            var Mod2 = new Program();
            //var Multiple_3 = new Program();
            // string OUT;

            // = "Слово|привет|пока|дочитал".Split('|');
            // var ArrString = new Program();

            #endregion
            //goto Z3;
            #region Задание 1
            /*Система подсчёта налогов. Пользователь вводит companiesNumber – число компаний, работающих в стране, а так же tax (%) – налог, который взымает государство. 
             * Если представить, что доход, который получает каждая из этих компании одинаковый и он равен companyRevenue, то какой суммарный налог государство получит?*/

            // goto Z4;
            Console.WriteLine("Задание 1");
            Console.WriteLine("Здравствуйте.");
            Console.WriteLine("Введите число компаний, работающих в стране");
            companiesNumber = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Введите доход одной компании");
            companyRevenue = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Введите налог, который взымает государство в %%");
            tax = Convert.ToInt32(Console.ReadLine());

            //Output = Convert.ToDouble(Mod2.GetTotalTax(companiesNumber, companyRevenue, tax));
            //Output = Convert.ToString(Mod2.GetTotalTax(companiesNumber, companyRevenue, tax));

            Console.WriteLine("суммарный налог государства =" + Convert.ToDouble(Mod2.GetTotalTax(companiesNumber, companyRevenue, tax)));
            //Console.WriteLine(OutputString);

            Console.WriteLine();

            // int TotalRevenueOUTOUT = GetTotalTax(companiesNumber, tax, companyRevenue);
            #endregion

            #region Задание 2
            /*Пользователь вводит N – натуральное число.  
             * Если N чётное и больше либо равно 18 – программа поздравляет его с совершеннолетием ("Поздравляю с совершеннолетием!").
             * Если N нечетное и меньше 18 но больше 12 – программа поздравляет с переходом в старшую школу ("Поздравляю с переходом в старшую школу!")
             * В остальных случаях программа просто поздравляет пользователя ("Поздравляю c N-летием!").*/

            Console.WriteLine("Задание 2");
            Console.WriteLine("Здравствуйте. Введите ваш возраст");
            age = Convert.ToInt32(Console.ReadLine());
            // OUT = OutputString.GetCongratulation(age);
            Console.WriteLine(Mod2.GetCongratulation(age));
            Console.WriteLine();
            Console.WriteLine("Задание 3");

        #endregion

        #region  Задание 3             
        Z3:
            /*Нужно перемножить 2 числа, представленных в виде строки. Необходимо предусмотреть тот факт, что пользователь может ввести, например, букву, а также учесть, что число может 
             * быть дробным, и для выделения её дробной части одни используют точку, другие – запятую.*/
            Console.WriteLine("Перемножим числа введённых вами в одной строке");
            Console.WriteLine("Введите 2 числа через пробел");

            UserInput = Console.ReadLine();

            try
            {
                var ArrString = UserInput.Split(' ');
                first = (ArrString[0]);
                second = (ArrString[1]);
                UserOutput3 = Mod2.GetMultipliedNumbers(first, second);

                Console.WriteLine(UserOutput3);
            }
            catch
            {
                Console.WriteLine("Пользователь не ввёл 2 числа через пробел");

            }
        #endregion

        #region Задание 4
        Z4:
            /*Программа для подсчета периметра и площади фигур (треугольник, прямоугольник, круг).
             * Пользователь выбирает фигуру, указывает, что программа будет считать – площадь или периметр.
             * - Все результаты округлить до целого числа.
             * Для треугольника:
             * - при расчёте по стороне и высоте использовать значение FirstSide*/
            // string usersInput4;


//  ---------------------- Новый

            var program = new Program(); 
            Figure figure = new Figure();
            Parameter parametеr = new Parameter(); 

            Console.WriteLine("Выберите фигуру:\n" +
                "1 - Triangle \n" +
                "2 - Rectangle \n" +
                "3 - Circle \n");
            if ((int.TryParse(Console.ReadLine(), out int choiseFigura)) && (choiseFigura > 0) && (choiseFigura < 4))
            {
                figure = (Figure)choiseFigura - 1;
            }

            Console.WriteLine("Выберите parametr:\n" +
                "1 - Square \n" +
                "2 - Perimeter \n");
            if ((int.TryParse(Console.ReadLine(), out int choiseParametr)) &&  (choiseParametr > 0) && (choiseParametr < 3))
            {
                parametеr = (Parameter)choiseParametr - 1;
            }

            var dimensions = new Dimensions()
            {
                //FirstSide = 4,
                //SecondSide = 3,
                //ThirdSide = 3,
                //Height = 3,
                //Diameter = 6,
                //Radius = 3
            };
            Console.Clear();
            Console.WriteLine("Введите FirstSide");
            dimensions.FirstSide = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите SecondSide");
            dimensions.SecondSide = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите ThirdSide");
            dimensions.ThirdSide = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите Height");
            dimensions.Height = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите Diameter");
            dimensions.Diameter = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите Radius");
            dimensions.Radius = Convert.ToDouble(Console.ReadLine());


            var result = program.GetFigureValues(figure, parametеr, dimensions);

            
            Console.WriteLine(result);

            #region Старый
            // - ----- Конец новый


            //int Case4Switch;

            //Console.ForegroundColor = ConsoleColor.Yellow;
            //Console.WriteLine("Выберите фигуру:");
            //Console.WriteLine("В зависимости от фигуры введите 1 2 или 3");

            //Console.WriteLine();

            //Console.ForegroundColor = ConsoleColor.Red;
            //Console.WriteLine("Треугольник  1");
            //Console.WriteLine("Квадрат      2");
            //Console.WriteLine("Круг         3");
            //Console.WriteLine();
            //Console.ResetColor();

            //usersInput4 = Console.ReadLine();

            //Dimensions a = new Dimensions();
            //Figure t = new Figure();
            //t = Figure.

            //try
            //{
            //    Case4Switch = Convert.ToInt32(usersInput4);
            //}

            //catch
            //{
            //    Console.ForegroundColor = ConsoleColor.Red;
            //    Console.WriteLine("Выбор не верен. Необходимо вводить только 1 2 или 3, в зависимости от фигуры.");
            //    Console.ResetColor();
            //}

            //Case4Switch = Convert.ToInt32(usersInput4);

            //switch (Case4Switch)
            //{
            //    case 0:
            //        Console.Clear();
            //        Console.ForegroundColor = ConsoleColor.Yellow;
            //        Console.WriteLine("Выбрали Треугольник");
            //        Console.ResetColor();

            //        break;

            //    case 1:
            //        Console.Clear();
            //        Console.ForegroundColor = ConsoleColor.Yellow;
            //        Console.WriteLine("Выбрали Квадрат");
            //        Console.ResetColor();

            //        break;

            //    case 2:
            //        Console.Clear();
            //        Console.ForegroundColor = ConsoleColor.Yellow;
            //        Console.WriteLine("Выбрали Окружность");
            //        Console.Clear();
            //        Console.WriteLine("Введите Радиус");

            //        var F3 = new Program(); // Создали объект класса Програм
            //        //var choise = new Program();
            //        int choise = Figure.Circle;

            //        //F3.GetFigureValues( );

            //        //Dimensions.FirstSide =   ;

            //        Console.ResetColor();

            //        break;

            //    default:
            //        Console.ForegroundColor = ConsoleColor.Red;
            //        Console.WriteLine("Выбор не верен. Необходимо вводить только 1 2 или 3, в зависимости от фигуры.");
            //        Console.ResetColor();
            //        break;
            //}
            #endregion Новый код

            #endregion
        }

        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            return companiesNumber * companyRevenue * tax / 100;
        }

        public string GetCongratulation(int input)
        {            
            if (input % 2 == 0 && input >=18)
            {
                return "Поздравляю с совершеннолетием!";
            }
            else if (input % 2 != 0 && input >= 12 && input < 18)
            {
                return "Поздравляю с переходом в старшую школу!";
            }
            else 
            {
                return "Поздравляю с " + input + "-летием!";
            }
        }

        public double GetMultipliedNumbers(string first, string second)
        {
            first = first.Replace('.', ',');
            second = second.Replace('.', ',');
            Double.TryParse(first, out double FF);
            Double.TryParse(second, out double SS);
            return FF * SS;
        }

        public double GetFigureValues(Figure figureType, Parameter parameterToCompute, Dimensions dimensions)
        {
            double square;
            double perimeter;

            switch (figureType)
            {
                case (Figure.Circle):

                    switch (parameterToCompute)
                    {
                        case (Parameter.Square):
                            if (dimensions.Radius != 0)
                            {
                                square = Math.PI * Math.Pow(dimensions.Radius, 2);
                                return Math.Round(square, 0);
                            }
                            else if (dimensions.Diameter != 0)
                            {
                                square = Math.PI * Math.Pow(dimensions.Diameter, 2) / 4;
                                return Math.Round(square, 0);
                            }
                            else
                            {
                                return 0;
                            }

                        case (Parameter.Perimeter):
                            if (dimensions.Radius != 0)
                            {
                                perimeter = 2 * Math.PI * dimensions.Radius;
                                return Math.Round(perimeter, 0);
                            }
                            else if (dimensions.Diameter != 0)
                            {
                                perimeter = Math.PI * dimensions.Diameter;
                                return Math.Round(perimeter, 0);
                            }
                            else
                            {
                                return 0;
                            }

                    }
                    break;

                case (Figure.Triangle):
                    switch (parameterToCompute)
                    {
                        case (Parameter.Square):
                            if (dimensions.Height != 0)
                            {
                                square = dimensions.Height * dimensions.FirstSide / 2;
                                return Convert.ToInt32(square);
                                // return Math.Round(square, 0);
                            }
                            else
                            {
                                double halfPerimeter = (dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide) / 2;
                                square = Math.Pow(halfPerimeter * (halfPerimeter - dimensions.FirstSide) * (halfPerimeter - dimensions.SecondSide) * (halfPerimeter - dimensions.ThirdSide), 0.5);
                                return Convert.ToInt32(square);
                            }

                        case (Parameter.Perimeter):
                            {
                                perimeter = dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide;
                                return Convert.ToInt32(perimeter);
                            }
                    }
                    break;
                case (Figure.Rectangle):
                    switch (parameterToCompute)
                    {
                        case (Parameter.Square):
                            
                                square = dimensions.SecondSide * dimensions.FirstSide;
                                // return Math.Round(square, 0);
                                return Convert.ToInt32(square);
                           

                        case (Parameter.Perimeter):
                            {
                                if (dimensions.SecondSide != 0)
                                {
                                    perimeter = dimensions.FirstSide * 2 + dimensions.SecondSide * 2;
                                    //return Math.Round(perimeter, 0);
                                    return Convert.ToInt32(perimeter);
                                }
                                else
                                {
                                    perimeter = dimensions.FirstSide * 4;
                                    return Convert.ToInt32(perimeter);
                                }
                            }

                    }
                    break;
            }

            return 0;
        }

    }
}
